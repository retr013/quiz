from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from accounts.models import User
from quiz.forms import ChoiceFormSet
from quiz.models import Test, Result, Question


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 'tests/list.html'
    context_object_name = 'tests'


class TestDetailView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    model = Test
    template_name = 'tests/details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'uuid'
    paginate_by = 5

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.model.objects.get(uuid=uuid)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.get_queryset(), **kwargs)
        return context

    def get_queryset(self):
        return Result.objects.filter(test=self.get_object()).select_related('test', 'user').order_by('state')


class TestResultDetailsView(LoginRequiredMixin, DetailView):
    model = Result
    template_name = 'results/details.html'
    context_object_name = 'result'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)


class TestResultCreateView(LoginRequiredMixin, CreateView):

    def post(self, request, uuid):
        result = Result.objects.create(
            test=Test.objects.get(uuid=uuid),
            user=request.user,
            state=Result.STATE.NEW,
            current_order_number=0
        )
        result.save()

        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result.uuid,
                # 'order_number': 1
            }
        ))


class TestResultQuestionView(LoginRequiredMixin, UpdateView):

    def get(self, request, uuid, result_uuid):

        order_number = Result.objects.get(uuid=result_uuid).current_order_number+1

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        choices = ChoiceFormSet(queryset=question.choices.all())

        return render(
            request=request,
            template_name='tests/question.html',
            context={
                'question': question,
                'choices': choices
            }
        )

    def post(self, request, uuid, result_uuid):

        order_number = Result.objects.get(uuid=result_uuid).current_order_number+1

        question = Question.objects.get(
            test__uuid=uuid,
            order_number=order_number
        )

        choices = ChoiceFormSet(data=request.POST)
        selected_choices = [
            'is_selected' in form.changed_data
            for form in choices.forms
        ]

        result = Result.objects.get(
            uuid=result_uuid
        )

        if selected_choices.count(True) == 0:
            messages.warning(self.request, 'Select at least one answer!')
            order_number -= 1
        elif selected_choices.count(True) == len(choices.forms):
            messages.warning(self.request, 'All answers cannot be selected!')
            order_number -= 1
        else:
            result.update_result(order_number, question, selected_choices)

        if result.state == Result.STATE.FINISHED:
            return HttpResponseRedirect(reverse(
                'tests:result_details',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                }
            ))
        else:
            return HttpResponseRedirect(reverse(
                'tests:question',
                kwargs={
                    'uuid': uuid,
                    'result_uuid': result.uuid,
                    # 'order_number': order_number+1
                }
            ))


class UserLeaderboard(LoginRequiredMixin, ListView):
    model = User
    template_name = 'tests/leaderboard.html'
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all().order_by('-rating').filter(rating__gt=0)
        return context


class TestResultUpdateView(LoginRequiredMixin, UpdateView):

    def get(self, request, uuid, result_uuid):
        result = Result.objects.get(uuid=result_uuid)

        return HttpResponseRedirect(reverse(
            'tests:question',
            kwargs={
                'uuid': uuid,
                'result_uuid': result_uuid,
                # 'order_number': result.current_order_number+1
            }
        ))


class ResultDeleteView(LoginRequiredMixin, DeleteView):
    model = Result
    success_url = reverse_lazy('tests:list')
    template_name = 'results/delete.html'
    pk_url_kwarg = 'result_uuid'

    def get_object(self):
        uuid = self.kwargs.get('result_uuid')
        return self.get_queryset().get(uuid=uuid)


def error_400(request, exception):
    data = {}
    return render(request, '400.html', data)


def error_403(request, exception):
    data = {}
    return render(request, '403.html', data)


def error_404(request, exception):
    data = {}
    return render(request, '404.html', data)


def error_500(request):
    data = {}
    return render(request, '500.html', data)

from django.urls import path

from quiz.views import TestDetailView, TestListView, TestResultQuestionView, TestResultCreateView, \
    TestResultDetailsView, UserLeaderboard, TestResultUpdateView, ResultDeleteView

app_name = 'tests'

urlpatterns = [
    path('', TestListView.as_view(), name='list'),


    path('<uuid:uuid>/', TestDetailView.as_view(), name='details'),
    path('leaderboard/', UserLeaderboard.as_view(), name='leaderboard'),
    path('<uuid:uuid>/results/create', TestResultCreateView.as_view(), name='result_create'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/delete', ResultDeleteView.as_view(), name='result_delete'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/update', TestResultUpdateView.as_view(), name='result_update'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/details', TestResultDetailsView.as_view(), name='result_details'),
    # path('<uuid:uuid>/results/<uuid:result_uuid>/questions/<int:order_number>', TestResultQuestionView.as_view(), name='question'),
    path('<uuid:uuid>/results/<uuid:result_uuid>/questions/next', TestResultQuestionView.as_view(), name='question'),
]

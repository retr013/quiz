def frange(*args):  # noqa
    if len(args) == 1:
        start, end, step = 0, args[0], 1
    elif len(args) == 2:
        start, end, step = *args, 1
    elif len(args) == 3:
        start, end, step = args[0], args[1], args[2]

    if step == 0:
        raise ValueError('frange() arg 3 must not be zero')

    cycle = 0

    while True:
        result = start + step * cycle
        if step > 0 and result >= end:
            break
        elif step < 0 and result <= end:
            break
        yield result
        cycle += 1


assert(list(frange(5)) == [0, 1, 2, 3, 4])
assert(list(frange(2, 5)) == [2, 3, 4])
assert(list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert(list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert(list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert(list(frange(1, 5)) == [1, 2, 3, 4])
assert(list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert(list(frange(0, 0)) == [])
assert(list(frange(100, 0)) == [])

print('ok1')







def imap(func, *args):
    for arg in zip(*args):
        yield func(*arg)


n = [23, 24, 25]


def test(integer):
    return integer**2


assert(list(imap(test, n)) == [529, 576, 625])
print('ok2')

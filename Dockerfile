#FROM python:3.8
#
#RUN apt update
#
#RUN mkdir /srv/project
#WORKDIR /srv/project
#
#COPY ./src ./src
#COPY ./commands ./commands
#COPY ./requirements.txt ./requirements.txt
#
#RUN ping google.com
#RUN pip install -r requirements.txt
#
#ENV TZ Europe/Kiev
#
#CMD ["bash"]

FROM python:3.8

RUN apt update

RUN mkdir /srv/project
WORKDIR /srv/project

COPY ./src ./src
COPY ./commands ./commands
COPY ./requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

#CMD ["python", "src/manage.py", "runserver", "0:8008"]
CMD ["bash"]